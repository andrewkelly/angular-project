import { Component, NgModule, OnInit } from '@angular/core';
import programsList from '../../../assets/json/programLists.json';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.scss']
})

export class ProgramsComponent implements OnInit {
  lang: string;
  public programList = programsList.programs;

  constructor() {

  }

  ngOnInit() {

  }

}
